# Battery Dev 2022

## About this repo

The context of this repository is the Battery Dev Hackaton 2022. See more at [battery.dev](www.battery.dev).

This is mainly an attempt to make a functional machine learning pipeline using [Tensorflow Extended (TFX)](https://www.tensorflow.org/tfx).


## Usage

The main work has been done in the `exploration.ipynb` note book. It uses an `InteractiveContext` to run the components of the pipeline.
Some of the cells are written to files and most of the code should be easy to port to an actual TFX pipeline that can run on Google Cloud.

### Dependencies

### DVC

The repo is is set up to use [DVC](https://dvc.org/). After installing dvc you can run `dvc pull` from the root of the repo to pull the training data in csv and TFRecord formats.

`dvc pull` should fetch the directory `data/` from a public GCP bucket.

#### Poetry

The python environment is created with [Poetry](https://python-poetry.org/docs/). Please follow the link for instructions on installing it.

Once Poetry is installed run the command `poetry install` from the root of the repo. Poetry will resolve and install the specified dependencies in `pyproject.toml`.

Now, `poetry run jupyter notebook` from the root of the repo to start the Jupyter server on localhost in the Poetry environment. Once you have navigated to the notebokk in your browser (defaults to localhost:8888), make sure you are using the `Python 3 (ipykernel)` kernel in Jupyter. If not you may not have the correct versions of the required Python packages.
