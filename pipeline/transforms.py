
from typing import Dict, Any

import tensorflow as tf
import tensorflow_transform as tft
from tensorflow.keras import layers
import numpy as np

def processed_name(key: str) -> str:
    return key + "_xf"


def preprocessing_fn(inputs: Dict) -> Dict:
    """
    tf.transform's callback function for preprocessing inputs.
    
    Args:
    inputs: map from feature keys to raw not-yet-transformed features.
    
    Returns:
    Map from string feature key to transformed feature operations.
    """
    freq_size = tft.size(inputs["frequency"])
    print("---------------")
    print(f"FREQ SIZE: {freq_size}")
    print("---------------")
    output = {}
    output[processed_name("frequency")] = tft.scale_to_0_1(inputs["frequency"])
    output[processed_name("impedance_real")] = tft.scale_to_0_1(inputs["impedance_real"])
    output[processed_name("impedance_imaginary")] = tft.scale_to_0_1(inputs["impedance_imaginary"])
    output[processed_name("circuit")] = tft.compute_and_apply_vocabulary(inputs["circuit"])

    return output
